package at.tugraz.nriedmann.learningagent.nlp;

import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.Classification;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.trees.Tree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class SentenceWithInformation {
    private final String sentence;
    private final List<WordTag> keyWordsWithType;
    private final Classification typeOfSpeechAct;

    private Tree parseTree;
    private List<String> namedEntities;
    private List<String> partOfSpeechTags;
    private List<String> keyPhrases;

    public Optional<String> getKeyVerb() {
        for (WordTag wordTag : keyWordsWithType) {
            if (wordTag.tag().contains("VB")) {
                return Optional.of(wordTag.word());
            }
        }
        return Optional.empty();
    }
}
