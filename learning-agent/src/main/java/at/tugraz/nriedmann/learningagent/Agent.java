package at.tugraz.nriedmann.learningagent;

import at.tugraz.nriedmann.learningagent.humaninterface.HumanToAgentInterface;
import at.tugraz.nriedmann.learningagent.knowledgebase.IKnowledgeBase;
import at.tugraz.nriedmann.learningagent.nlp.NLPComponent;
import at.tugraz.nriedmann.learningagent.nlp.SentenceWithInformation;
import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.ISpeechActClassifier;
import edu.stanford.nlp.ling.WordTag;
import lombok.extern.slf4j.Slf4j;
import org.apache.jena.rdf.model.Resource;

import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
public class Agent implements Runnable {

    private final IKnowledgeBase knowledgeBase;
    private final HumanToAgentInterface humanToAgentInterface;

    public Agent(IKnowledgeBase knowledgeBase, NLPComponent nlpComponent, ISpeechActClassifier speechActClassifier) {
        this.knowledgeBase = knowledgeBase;

        this.humanToAgentInterface = new HumanToAgentInterface(nlpComponent, speechActClassifier);
    }

    @Override
    public void run() {
        log.info("Learning Agent started");
        boolean running = true;

        humanToAgentInterface.writeWelcomeMessage();

        while (running) {
            String inputLine = humanToAgentInterface.read();
            if (humanToAgentInterface.isExitCommand(inputLine)) {
                humanToAgentInterface.write("Ok. Trying to persist what I know first...");
                knowledgeBase.persistCurrentState();
                humanToAgentInterface.write("Goodbye!");
                running = false;
            } else {
                Optional<SentenceWithInformation> optionalParsedSentence = humanToAgentInterface.handleInput(inputLine);

                if (optionalParsedSentence.isPresent()) {
                    SentenceWithInformation parsedSentence = optionalParsedSentence.get();

                    /**TODO take certainty into account. Is 1.0 all the time at the moment anyway {@link at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.SimpleSpeechActClassifier} **/

                    switch (parsedSentence.getTypeOfSpeechAct().getSpeechAct()) {
                        case CONFIRMATION:
                            break;
                        case DISCONFIRMATION:
                            break;
                        case INFORMATION:
                            //TODO write to KB
                            break;
                        case REQUEST:
                            //TODO check all actions are known in KB
                            String nameOfRequestedAction = getActionNameFromSentence(parsedSentence);
                            Optional<Resource> optionalResource = knowledgeBase.getAction(nameOfRequestedAction);
                            if (optionalResource.isPresent()) {
                                //known action
                                Resource requestedAction = optionalResource.get();
                                humanToAgentInterface.write("I know this action. It's " + requestedAction);
                                //TODO check all objects are known in KB
                                //TODO check if plan can be found using facts and actions in KB
                            } else {
                                //unknown action
                                humanToAgentInterface.write(
                                        "I don't know how to \" " +
                                                parsedSentence.getSentence() +
                                                " \" yet! \n Please define the action \" " +
                                                nameOfRequestedAction +
                                                " \""// using STRIPS!"
                                );
                            }

                            break;
                    }
                }

            }
        }
        log.info("Learning Agent stopped");
    }

    private String getActionNameFromSentence(SentenceWithInformation sentenceWithInformation) {
        String actionName = "ACTION:";
        for (WordTag wordTag : sentenceWithInformation.getKeyWordsWithType()) {
            actionName +=wordTag.word()+"_";
        }
        actionName = actionName.substring(0, actionName.length()-1);
        return actionName;
    }

}
