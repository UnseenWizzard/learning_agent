package at.tugraz.nriedmann.learningagent.knowledgebase;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.NotFoundException;
import org.apache.jena.util.FileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

public class JenaRDFKnowledgeBase implements IKnowledgeBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(JenaRDFKnowledgeBase.class);

    private final Model knowledgeBase;
    private final String knowledgeBaseFilePath;

    public static Property IS_ACTION, ACTION_PRECONDITION, ACTION_OBJECT, ACTION_EFFECT;


    public JenaRDFKnowledgeBase(String filepath) {
        knowledgeBaseFilePath = filepath;

        Model loadedKB;
        try {
            loadedKB = FileManager.get().loadModel(knowledgeBaseFilePath);
        } catch (Exception e) {
            LOGGER.error("Unable to load model from file {}. Starting with empty model!", knowledgeBaseFilePath);
            loadedKB = ModelFactory.createDefaultModel();

        }
        knowledgeBase = loadedKB;
        createProperties();
    }

    private void createProperties() {
        IS_ACTION = knowledgeBase.createProperty("isAction");
        ACTION_PRECONDITION = knowledgeBase.createProperty("precondition");
        ACTION_OBJECT = knowledgeBase.createProperty("object");
        ACTION_EFFECT = knowledgeBase.createProperty("effect");
    }

//    private void loadProperties(Model model) {
//        IS_ACTION = model.getProperty("isAction");
//        ACTION_PRECONDITION = model.getProperty("precondition");
//        ACTION_OBJECT = model.getProperty("object");
//        ACTION_EFFECT = model.getProperty("effect");
//    }

    @Override
    public boolean addAction(String name, Map<Property, String> properties) {
        Resource action = knowledgeBase.createResource(name);
        for (Map.Entry<Property, String> entry : properties.entrySet()) {
            action.addProperty(entry.getKey(), entry.getValue());
        }
        return true;
    }

    @Override
    public Optional<Resource> getAction(String name) {
        Model m = ModelFactory.createDefaultModel();
        if (knowledgeBase.contains(m.createResource(name), null)) {
            return Optional.of(knowledgeBase.getResource(name));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean persistCurrentState() {
        try {
            File outputFile = new File(knowledgeBaseFilePath);
            outputFile.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            knowledgeBase.write(outputStream);
            return true;
        } catch (FileNotFoundException e) {
            LOGGER.error("Could not open file {} for writing. Unable to persist KB!", knowledgeBaseFilePath);
        } catch (IOException e) {
            LOGGER.error("Could not create new file {} to write to. Unable to persist KB!", knowledgeBaseFilePath);
        }
        return false;
    }
}
