package at.tugraz.nriedmann.learningagent.humaninterface;


import at.tugraz.nriedmann.learningagent.nlp.NLPComponent;
import at.tugraz.nriedmann.learningagent.nlp.SentenceWithInformation;
import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.Classification;
import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.ISpeechActClassifier;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.trees.Tree;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
public class HumanToAgentInterface {

    private static final List<String> exitStrings = Arrays.asList("exit", "quit", "q");

    private Console console;
    private BufferedReader inputReader;
    private BufferedWriter outputWriter;

    private final NLPComponent nlpComponent;
    private final ISpeechActClassifier speechActClassifier;

    public HumanToAgentInterface(NLPComponent nlpComponent, ISpeechActClassifier speechActClassifier) {
        this.nlpComponent = nlpComponent;
        this.speechActClassifier = speechActClassifier;

        console = System.console();
        if (console == null) {
            log.error("Could not get System.console() to read/write, using stdin/out!");
            inputReader = new BufferedReader(new InputStreamReader(System.in));
            outputWriter = new BufferedWriter(new OutputStreamWriter(System.out));
        }
    }

    public String read() {
        if (console != null) {
            return console.readLine();
        } else {
            try {
                return inputReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    public void write(String output) {
        if (console != null) {
            console.writer().print(output + "\n");
            console.flush();
        } else {
            try {
                outputWriter.write(output + "\n");
                outputWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Optional<SentenceWithInformation> handleInput(String inputLine){
        if (inputLine.isEmpty()) {
            log.warn("Input line was empty...");
            return Optional.empty();
        }

        write("I heard \"" + inputLine + "\". Trying to understand that..");

        //consists of is not handled like contains, no list from keywords, try replacing if encountered..
        if (inputLine.contains("consists of")) {
            inputLine = inputLine.replace("consists of", "contains");
            write(" I replaced 'consists of' with 'contains' to help me understand this!");
        }

        if (inputLine.contains("consist of")) {
            inputLine = inputLine.replace("consist of", "contain");
            write(" I replaced 'consist of' with 'contain' to help me understand this!");
        }


        log.info("Got Input {}", inputLine);
        Tree parseTree = nlpComponent.getParseTree(inputLine);
        List<String> namedEntities = nlpComponent.getNamedEntities(inputLine);
        List<String> partOfSpeechTags = nlpComponent.getPartOfSpeechTags(inputLine);
        List<String> keyPhrases = nlpComponent.getKeyPhrases(inputLine);

        List<WordTag> keyWordsWithType = nlpComponent.parseAndGetKeyPhraseTokens(inputLine);
        Classification typeOfSpeechAct = speechActClassifier.classifySentence(inputLine);

        write("  found key words: " + keyWordsWithType);
        write("  I think this is a: " + typeOfSpeechAct);

        return Optional.of(new SentenceWithInformation(inputLine, keyWordsWithType, typeOfSpeechAct, parseTree, namedEntities, partOfSpeechTags, keyPhrases));

    }

    public void writeWelcomeMessage() {
        write("Hello there!");
        write("I try to fulfill the actions you tell me to perform. I might need to ask for your help though.");
        write("If you want to quit, type 'quit' or 'q'\n");
        write("What can I do for you?");
    }

    public boolean isExitCommand(String inputLine) {
        return exitStrings.contains(inputLine);
    }
}
