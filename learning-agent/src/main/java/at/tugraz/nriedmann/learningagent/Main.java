package at.tugraz.nriedmann.learningagent;

import at.tugraz.nriedmann.learningagent.knowledgebase.JenaRDFKnowledgeBase;
import at.tugraz.nriedmann.learningagent.nlp.NLPComponent;
import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.ISpeechActClassifier;
import at.tugraz.nriedmann.learningagent.nlp.speechact.classifier.SimpleSpeechActClassifier;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Properties;

@Slf4j
public class Main {

    private static final String KB_PROPERTY_KEY = "knowledgebase_file_path";
    private static final String DEFAULT_KB_FILEPATH = "learning_agent_KB.xml";

    public static void main(String[] args) {
        Properties applicationProperties = new Properties();
        try {
            applicationProperties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
        } catch (IOException e) {
            log.error("Could not load application properties! Please make sure the application.properties resource exists. Exiting...");
            System.exit(-1);
        }

        String knowledgeBaseFilePath = applicationProperties.getProperty(KB_PROPERTY_KEY);
        if (knowledgeBaseFilePath == null) {
            log.error("Could not load KB filepath from properties. Using default ({}). Is %s entry present?", DEFAULT_KB_FILEPATH, KB_PROPERTY_KEY);
            knowledgeBaseFilePath = DEFAULT_KB_FILEPATH;
        }

        NLPComponent nlpComponent = new NLPComponent();
        ISpeechActClassifier speechActClassifier = new SimpleSpeechActClassifier(nlpComponent);

        JenaRDFKnowledgeBase knowledgeBase = new JenaRDFKnowledgeBase(knowledgeBaseFilePath);

        Agent learningAgent = new Agent(knowledgeBase, nlpComponent, speechActClassifier);

        learningAgent.run();

        //TODO any cleanup needed when done?

        System.exit(0);
    }
}
