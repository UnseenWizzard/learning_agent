package at.tugraz.nriedmann.learningagent.knowledgebase;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.Map;
import java.util.Optional;

public interface IKnowledgeBase {

    //TODO add and get based on jena api
    boolean addAction(String name, Map<Property,String> properties);

    Optional<Resource> getAction(String name);

    boolean persistCurrentState();
}
