package at.tugraz.nriedmann.learningagent.nlp.speechact.classifier;

public class Classification {

    public enum SpeechAct {
        CONFIRMATION,
        DISCONFIRMATION,
        INFORMATION,
        REQUEST
    }

    private SpeechAct speechAct;
    private double certainty;

    public Classification(SpeechAct speechAct, double certainty) {
        this.speechAct = speechAct;
        this.certainty = certainty;
    }

    public SpeechAct getSpeechAct() {

        return speechAct;
    }

    public double getCertainty() {
        return certainty;
    }

    @Override
    public String toString() {
        return speechAct.name() + "[" + certainty + "]";
    }
}
