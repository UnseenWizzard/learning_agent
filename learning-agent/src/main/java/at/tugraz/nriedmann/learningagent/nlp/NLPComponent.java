package at.tugraz.nriedmann.learningagent.nlp;

import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.WordTag;
import edu.stanford.nlp.simple.*;
import edu.stanford.nlp.trees.Tree;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class NLPComponent {

    public Tree getParseTree(String input) {
        Sentence sentence = new Sentence(input);
        Tree parseTree = sentence.parse();
        log.info("parseTree: {}", parseTree);
        return parseTree;
    }

    public List<String> getNamedEntities(String input) {
        Sentence sentence = new Sentence(input);
        List<String> namedEntities = sentence.nerTags();
        log.info(" NamedEntities: {}", namedEntities);
        return namedEntities;
    }

    public List<String> getPartOfSpeechTags(String input) {
        Sentence sentence = new Sentence(input);
        List<String> posTags = sentence.posTags();
        log.info(" PartsOfSpeech: {}" , posTags);
        return posTags;
    }

    public List<String> getKeyPhrases(String input) {
        Sentence sentence = new Sentence(input);
        List<String> keyphrases = sentence.algorithms().keyphrases();
        log.info("    KeyPhrases: {}", keyphrases);
        return keyphrases;
    }

    public List<WordTag> parseAndGetKeyPhraseTokens(String input) {
        Sentence sentence = new Sentence(input);
        Tree parseTree = sentence.parse();
        List<String> keyPhrases = sentence.algorithms().keyphrases();

        List<WordTag> keyWordsWithType = new ArrayList<>();

        List<WordTag> wordsInSentence = walkParseTree(parseTree, parseTree);

        for (String keyPhrase : keyPhrases) {
            List<String> types = new ArrayList<>();
            for (WordTag word : wordsInSentence) {
                if (containsFullWord(keyPhrase, word.word())) {
                    types.add(word.tag());
                }
            }
            if (!types.isEmpty()) {
                keyWordsWithType.add(new WordTag(keyPhrase, types.toString()));
            }
        }

        List<WordTag> extendedReturnList = new ArrayList<>();
        int keyPhraseIndex = 0;

        for (WordTag word : wordsInSentence) {
            if (keyPhraseIndex < keyWordsWithType.size() &&
                    !extendedReturnList.contains(keyWordsWithType.get(keyPhraseIndex)) &&
                    containsFullWord(keyWordsWithType.get(keyPhraseIndex).word(),word.word())) {
                extendedReturnList.add(keyWordsWithType.get(keyPhraseIndex));
                keyPhraseIndex++;
            } else if (word.tag().contains("VB") && !keyPhrases.contains(word.word())) {
                extendedReturnList.add(word);
            }
        }

        log.info("KeyPhrase Tokens: {}", extendedReturnList);

        return extendedReturnList;
    }

//    a sandwich contains rye bread, hot salami and a slice of a french cheese
//    a slice of hot salami is on the kithcen counter

    private static boolean containsFullWord(String string, String word){
        String patternString = "\\b"+word+"\\b";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    private List<WordTag> walkParseTree(Tree tree, Tree root) {
        List<WordTag> words = new ArrayList<>();

        if (tree.isPreTerminal()) {
            words.add(new WordTag(tree.getLeaves().get(0).value(), tree.value()));
        }


        for (Tree child : tree.children()) {
            words.addAll(walkParseTree(child, tree));
        }
        return words;
    }
}
