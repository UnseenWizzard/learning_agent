package at.tugraz.nriedmann.learningagent.nlp.speechact.classifier;

public interface ISpeechActClassifier {

    Classification classifySentence(String sentence);

}
