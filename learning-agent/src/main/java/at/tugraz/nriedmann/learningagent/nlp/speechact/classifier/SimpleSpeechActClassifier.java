package at.tugraz.nriedmann.learningagent.nlp.speechact.classifier;

import at.tugraz.nriedmann.learningagent.nlp.NLPComponent;
import edu.stanford.nlp.ling.WordTag;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class SimpleSpeechActClassifier implements ISpeechActClassifier {

    private NLPComponent nlpComponent;

    private final static List<String> confirmationWords = Arrays.asList("yes", "true", "ok");
    private final static  List<String> disconfirmationWords = Arrays.asList("no", "false");
    private final static  List<String> informationWords = Arrays.asList("is", "are", "consists", "contains", "in");
    private final static  List<String> requestWords = Arrays.asList("make", "get", "do", "put", "spread");

    public SimpleSpeechActClassifier(NLPComponent nlpComponent) {
        this.nlpComponent = nlpComponent;
    }


    @Override
    public Classification classifySentence(String sentence) {
        List<WordTag> keyPhraseTokens = nlpComponent.parseAndGetKeyPhraseTokens(sentence);
        Classification classification = null;
        String classifierWord = null;
        for (WordTag wordWithTag : keyPhraseTokens) {
            if (wordWithTag.tag().contains("VB")) {
                classifierWord = wordWithTag.word();
                if (confirmationWords.contains(wordWithTag.word().toLowerCase())) {
                    classification = new Classification(Classification.SpeechAct.CONFIRMATION, 1.0);
                    break;
                } else if (disconfirmationWords.contains(wordWithTag.word().toLowerCase())) {
                    classification = new Classification(Classification.SpeechAct.DISCONFIRMATION, 1.0);
                    break;
                } else if (informationWords.contains(wordWithTag.word().toLowerCase())) {
                    classification = new Classification(Classification.SpeechAct.INFORMATION, 1.0);
                    break;
                } else if (requestWords.contains(wordWithTag.word().toLowerCase())) {
                    classification = new Classification(Classification.SpeechAct.REQUEST, 1.0);
                    break;
                }
                classifierWord = null;
            }
        }

        log.info("Classified as {} because of word {}", classification, classifierWord);
        return classification;
    }
}
